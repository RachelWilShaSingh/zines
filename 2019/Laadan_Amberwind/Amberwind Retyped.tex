\documentclass[a4paper,oneside]{book}
\usepackage[utf8]{inputenc}
\usepackage{mdframed}
\usepackage{makecell}
\usepackage{rotating}
\usepackage{minibox}
\usepackage{adjustbox}
\title{Amberwind's Láadan Lessons}
\author{compiled by rachel singh}
\date{\today}
\begin{document}
    \renewcommand{\chaptername}{Lesson}

    \newcommand{\laadan}[1]
    {
        % https://tex.stackexchange.com/questions/25249/how-do-i-use-a-particular-font-for-a-small-section-of-text-in-my-document
        \fontfamily{pbk}\selectfont{
        \textbf{ \textit{#1} }
        }
        \fontfamily{qcr}\selectfont{}
    }

    \newenvironment{solution}
    {   % Head
        \begin{mdframed}
            \begin{adjustbox}{angle=180}
                \small


    }
    {   % Foot
            \end{adjustbox}
        \end{mdframed}
    }

    \newcommand{\fitb}[1]
    {
        $\rule{#1}{0.15mm}$
    }

    \newcommand\tab[1][1cm]
    {
        \hspace*{#1}
    }

    \tableofcontents

    % -- LESSON ------------------------------------------------------ %
    \chapter{Pronunciation \& Transliteration}

        \section*{Pronunciation}

        Láadan was constructed to be simple to pronounce. This description is tailored for
        speakers of English because the material is written in English; but the sound system has
        been designed to present as few difficulties as possible, no matter what the native language
        of the learner.

        ~\\
        \begin{tabular}{l c l}
            Vowels: & \laadan{a} & as in ``c\underline{a}lm'' \\
                    & \laadan{e} & as in ``b\underline{e}ll'' \\
                    & \laadan{i} & as in ``b\underline{i}t'' \\
                    & \laadan{o} & as in ``h\underline{o}me'' \\
                    & \laadan{u} & as in ``s\underline{oo}n''
        \end{tabular}
        ~\\

        When a Láadan vowel is written with an accent mark above it, it is a vowel with high
        tone. English doesn’t have any tones, but that will be no problem for you, since you can
        express it as heavy stress. Think of the way that you distinguish the noun “convert” from
        the verb “convert” by stressing one of the two syllables. If you pronounce a high-toned
        Láadan vowel as you would pronounce a strongly-stressed English syllable, you will achieve
        the same effect as high tone. Because Láadan does not use English stress, this will not be a
        source of confusion.

        ~\\
        \begin{tabular}{l l l}
            Consonants:     & \laadan{b, d, sh, m, n, l, r, w, y, h} & as in English \\
                            & \laadan{th} & as in ``\underline{th}ink'' \\
                            & \laadan{zh} & as in ``plea\underline{s}ure'' \\
                            & \laadan{lh} & see below
        \end{tabular}
        ~\\

        There is one more consonant in Láadan: it is “lh” and it has no English equivalent. If
        you put the tip of your tongue firmly against the roof of your mouth at the point where it
        begins to arch upward, draw the corners of your lips back as you would for an exaggerated
        smile, and try to say English “sh,” the result should be an adequate “lh.” It is a sound with a
        hissing quality, and is not especially pleasant to hear. In Láadan it occurs only in words that
        are themselves references to something unpleasant, and can be added to words to give them
        a negative meaning. This is patterned after a similar feature of Navajo, and is something so
        very handy that I have always wished it existed in English.

        \section*{Transliteration}

        ``Transliteration'' is the term that refers to ``translating'' the sounds (not the meanings—
        nor the letters) of one language into another. Láadan has a rather limited (from an English
        standpoint) set of sounds into which to transliterate foreign words. Couple this with Láadan’s
        requirement that vowel-sounds and consonant-sounds occur in strict alternation, and many, if
        not most, foreign words are likely to come out sounding very different from their originals.
        We could start with some names. In English, names have no inherent meanings (or the
        meanings English names used to have are no longer considered relevant), so they present an
        excellent opportunity to practice transliteration.

        One further point. The Láadan sounds ``h,'' ``w,'' and “y” are not permitted at the end of a word.

        \subsection*{English-Láadan Sound Correspondences}

        What follows below is a rough chart of Láadan sounds to substitute for English sounds.
        There will be some variation due to English consonant-clusters, vowel-clusters and other non-
        Láadan-compatible formations. ``Ø'' means that it might be possible to simply omit that sound
        for transliteration purposes.

        ~\\
        \begin{tabular}{l l | l l | l l}
            a (pat)     & \laadan{a, e} &
            h (hot)     & \laadan{h} &
            s (daisy)   & \laadan{zh} \\

            a (cake)    & \laadan{e} &
            i (bit)     & \laadan{i} &
            sh          & \laadan{sh} \\

            a (father)  & \laadan{a} &
            i (bite)    & \laadan{a} &
            t           & \laadan{th} \\

            b           & \laadan{b} &
            i (machine) & \laadan{i} &
            u (uh)      & \laadan{a} \\

            c (certain) & \laadan{sh} &
            j           & \laadan{zh} &
            u (cue)     & \laadan{ya, u} \\

            cat (cat)   & \laadan{h, sh, Ø} &
            k           & \laadan{h, sh, Ø} &
            u (soon)    & \laadan(u) \\

            ch (chat)   & \laadan{sh} &
            l           & \laadan{l} &
            v (very)    & \laadan{b, w} \\

            ch (machine) & \laadan{sh} &
            m           & \laadan{m} &
            w (want)    & \laadan{w} \\

            ch (Bach)   & \laadan{h, sh, Ø} &
            n           & \laadan{n} &
            x (axe)     & \laadan{h} \\

            d           & \laadan{d} &
            ng          & \laadan{n} &
            y (you)     & \laadan{y} \\

            e (pet)     & \laadan{e} &
            o (pot)     & \laadan{a, o} &
            y (cygnet)  & \laadan{i} \\

            e (meet)    & \laadan{i} &
            o (boat)    & \laadan{o} &
            y (type)    & \laadan{a} \\

            e (hey)     & \laadan{e} &
            p           & \laadan{b} &
            y (marry)   & \laadan{i} \\

            f           & \laadan{h, sh} &
            q           & \laadan{h, sh, Ø} &
            z (zoo)     & \laadan{zh} \\

            g (get)     & \laadan{d, b, h, zh, Ø} &
            r           & \laadan{r} &
            z (azure)   & \laadan{zh} \\

            g (beige)   & \laadan{zh} &
            s (Sam)     & \laadan{sh}
        \end{tabular}

        \subsubsection*{Examples}

        ~\\
        \begin{tabular}{l l p{10cm}}
            Mary        & \laadan{Méri}         & ``Mary'' transliterates very easily. The consonant-vowel alternation is already present, and none of the phonemes (that’s the linguists’ name for “minimal units of sound”) is far from one found in Láadan. We should raise the tone of the first syllable to simulate the English stress-pattern. \\
            Anna        & \laadan{Ána}          & Again, the consonant-vowel alternation is already present, and the phonemes are easily substituted. \\
            Beth        & \laadan{Beth}         & This is a direct transliteration. But it does have the drawback that this word already has a couple of other meanings—look out for puns. \\
            Bethany     & \laadan{Bétheni}      & This is almost as direct as ``Beth;'' an accent on the first syllable will simulate the stress pattern of English. Láadan doesn’t have a ``shwa'' vowel (the one between the ``th'' and the ``n''). \\
            \makecell{Marsha, \\Marcia}        & \laadan{\makecell{Másha*, \\Máresha}}     & We have a couple of options here. We could insert a vowel to separate the ``r'' from the ``sh'' or we could drop one of them. \\
            Elizabeth   & \laadan{Elízhabeth}   & This one is easy! \\
            Carol       & \laadan{Hérel}        & Remember, we’re after the \textit{sound}, not the \textit{spelling}. \\
            Teresa      & \laadan{Therísha}     & Another easy, one-for-one transliteration. \\
            Margaret    & \laadan{\makecell{Mázhareth*, \\Máhareth}}
                                                & The ``rgr'' consonant cluster in the prevalent American English pronunciation of this name presents real difficulty. The fact that it involves the very problematic ``g'' further complicates matters. \\
            Suzette     & \laadan{Shuzhéth}     & How could we omit the founding mother of Láadan? \\
        \end{tabular}
        ~\\

        Below are some male names and a couple of place-names.

        ~\\
        \begin{tabular}{l p{2cm} p{10cm}}
            Michael     & \laadan{\makecell{Máhel, \\Máshel, \\Máyel*}}
                                                & asdf \\
            Matthew     & \laadan{Máthu}        & asdf \\
            Anthony     & \laadan{\makecell{Ánethoni\\Ánetheni*}}
                                                & asdf \\
            Steven      & \laadan{\makecell{Shíwen\\Thíben*\\Shethíben}}
                                                & asdf \\
            Arkansas    & \laadan{\makecell{Arahanesha\\Aranesha*}}
                                                & asdf \\
            California  & \laadan{Halishóna}    & asdf \\
        \end{tabular}
        ~\\

        Several of the names above have more than one form. All are valid, formally, but some are more euphonious than others—and some are more typically ``Láadan'' than their fellows. These are marked with asterisks (*) and will appear in the lessons to follow.

        \subsection*{Exercise}

        Consider your own name or names and those of your friends and family members. How would they transliterate? How do the transliterations sound?

    % -- LESSON ------------------------------------------------------ %
    \chapter{Word Order}

        \section*{Vocabulary}

        ~\\
        \begin{tabular}{l p{10cm}}
            \laadan{áya}        & to be beautiful      \\
            \laadan{Bíi}        & Type-of-Sentence Word—declarative      \\
            \laadan{doth}       & to follow      \\
            \laadan{hal}        & to work      \\
            \laadan{hena}       & sibling (by birth)      \\
            \laadan{mid}        & animal; any creature      \\
            \laadan{óoha}       & to be tired; to be weary      \\
            \laadan{thul}       & parent      \\
            \laadan{wa}         & Evidence Word—claimed to be true because the speaker herself perceived what has been said     \\
            \laadan{with}       & person      \\
        \end{tabular}

        Note that Láadan doesn’t divide adjectives and verbs into two classes as English does. Thus ``áya'' means ``be beautiful'' without any need for a separate word ``be'' in the sentence

        \subsection*{Word Order 1}

        NOTE: If you find grammar-geekiness intimidating, skip to ``Word Order 2.''

        \begin{center}
            [Type-of-Sentence Word, Verb, Case Phrase–Subject, Evidence Word]
        \end{center}

        Don’t be concerned about the notation above; it will be useful in the long run. A ``Case Phrase'' is the same thing as what traditional English grammars call a ``prepositional phrase.'' In English this means a preposition and its following noun phrase, as in ``with a hatchet'' or ``to the beach,'' most of the time; in Láadan it usually means a noun phrase and its ending. This will become clear as we go along, and each of the sentence patterns explained will use the notation, with ``Case Phrase'' abbreviated to just ``CP'' in future to save space. ``Case Phrase–Subject'' will be written ``CP–S.''

        \subsection*{Word Order 2}

        0 Láadan sentence begins with a word, called the [Type-of-Sentence Word], that tells you what sort of sentence it is—statement, question, request, etc. The most common of these words is “Bíi,” which begins ordinary statements (known grammatically as declarative sentences).

        1 Láadan sentence ends with a word, called the [“Evidence Word,”] that states why the speaker considers the sentence to be true. Probably the most common of these words is “wa,” which means “claimed to be true because the speaker herself perceived whatever has been said.”

        Within the sentence that begins with the Type-of-Sentence Word and ends with the Evidence Word, the Verb comes before the Noun(s).

        \subsection*{Examples}

        ~\\
        \begin{tabular}{l l}
            \laadan{Bíi áya hena wa.}           & (The/A) sibling is beautiful.          \\
            \laadan{Bíi doth mid wa.}           & (The/An) animal follows.          \\
            \laadan{Bíi hal thul wa.}           & (The/A) parent works.          \\
            \laadan{Bíi óoha with wa.}          & (The/A) person is weary.          \\
        \end{tabular}
        ~\\

        Note that Láadan has no separate words for “a(n)” or “the.” In future examples, one or the other will be provided for the purposes of the English translation.

        ~\\
        \begin{tabular}{l l}
            \laadan{Bíi áya Bétheni wa.} & Bethany is beautiful.
        \end{tabular}
        ~\\

        Note that names occupy the noun’s place in a sentence, just like any other noun.

        \section*{Exercises}

        You will find worksheet space immediately following each set of exercises. My answers follow the worksheet.

        \subsection*{Translate the following from Láadan to English}

        \begin{enumerate}
            \item[1.] \laadan{Bíi doth hena wa.}    ~\\ ~\\ English: \fitb{10cm}
            \item[2.] \laadan{Bíi hal mid wa.}      ~\\ ~\\ English: \fitb{10cm}
            \item[3.] \laadan{Bíi óoha thul wa.}    ~\\ ~\\ English: \fitb{10cm}
            \item[4.] \laadan{Bíi áya with wa.}     ~\\ ~\\ English: \fitb{10cm}
            \item[5.] \laadan{Bíi doth Másha wa.}   ~\\ ~\\ English: \fitb{10cm}
            \item[6.] \laadan{Bíi hal hena wa.}     ~\\ ~\\ English: \fitb{10cm}
        \end{enumerate}

        \subsection*{Put these in correct Láadan word order, then translate them into English.}

        \begin{enumerate}
            \item[7.]  \laadan{mid wa bíi óoha}          ~\\ ~\\ Correct order: \fitb{9cm} ~\\ English: \fitb{10cm}
            \item[8.]  \laadan{bíi thul áya wa}          ~\\ ~\\ Correct order: \fitb{9cm} ~\\ English: \fitb{10cm}
            \item[9.]  \laadan{doth with bíi wa}         ~\\ ~\\ Correct order: \fitb{9cm} ~\\ English: \fitb{10cm}
            \item[10.] \laadan{wa Elízhabeth bíi hal}    ~\\ ~\\ Correct order: \fitb{9cm} ~\\ English: \fitb{10cm}
            \item[11.] \laadan{hena bíi óoha wa}         ~\\ ~\\ Correct order: \fitb{9cm} ~\\ English: \fitb{10cm}
            \item[12.] \laadan{áya bíi wa mid}           ~\\ ~\\ Correct order: \fitb{9cm} ~\\ English: \fitb{10cm}
        \end{enumerate}

        \subsection*{Translate these from English to Láadan.}

        \begin{enumerate}
            \item[13.] The parent follows.       ~\\ ~\\ Láadan: \fitb{10cm}
            \item[14.] The woman works.          ~\\ ~\\ Láadan: \fitb{10cm}
            \item[15.] Margaret is weary.        ~\\ ~\\ Láadan: \fitb{10cm}
            \item[16.] The parent is beautiful.  ~\\ ~\\ Láadan: \fitb{10cm}
            \item[17.] A person follows.         ~\\ ~\\ Láadan: \fitb{10cm}
            \item[19.] Michael works.            ~\\ ~\\ Láadan: \fitb{10cm}
        \end{enumerate}

        \subsection*{Solutions}

        \begin{solution}
            \begin{tabular}{p{3.3cm} | p{3.3cm} | p{3.3cm}}
                1. A sibling follows. &
                2. The animal works. &
                3. The parent is tired. \\ \\
                4. The person is beautiful. &
                5. Marsha follows. &
                6. The sibling works. 
                
                \\ \\

                7. Bíi óoha mid wa. The creature is weary. &
                8. Bíi áya thul wa. The parent is beautiful. &
                9. Bíi doth with wa. The person follows \\ \\
                10. Bíi hal Elízhabeth wa. Elizabeth works. &
                11. Bíi óoha hena wa. A sibling is tired.  &
                12. Bíi áya mid wa. The animal is beautiful. 
                
                \\ \\

                13. Bíi doth thul wa.  &
                14. Bíi hal with wa. &
                15. Bíi óoha Mázhareth wa. \\ \\
                16. Bíi áya thul wa. &
                17. Bíi doth with wa. &
                18. Bíi hal Máyel wa. \\ \\
            \end{tabular}
        \end{solution}


    % -- LESSON ------------------------------------------------------ %
    \chapter{Vocabulary Interlude 1}

    Periodically, we’ll have a lesson with no grammar at all. These will be times to rehearse what we’ve learned so far and to add some new vocabulary with which to ``play.''

    \section*{Vocabulary}

    ~\\
    \begin{tabular}{l p{5cm} l p{5cm}}
        \laadan{áana}       & sleep &
        \laadan{rahu}       & to be closed \\

        \laadan{áath}       & door &
        \laadan{rana}       & drink; beverage \\

        \laadan{bud}        & asdf &
        \laadan{shum}       & tasdfasdf \\

        \laadan{bud}        & clothing &
        \laadan{shum}       & air \\

        \laadan{ede}        & grain &
        \laadan{tham}       & circle \\

        \laadan{héeya}      & to fear &
        \laadan{thili}      & fish \\

        \laadan{hesh}       & grass &
        \laadan{thom}       & pillow \\

        \laadan{i}          & and &
        \laadan{u}          & to be open \\

        \laadan{lom}        & song &
        \laadan{ud}         & stone \\

        \laadan{mime}       & to ask &
        \laadan{wíi}        & to be alive; to be living \\

        \laadan{muda}       & pig &
        \laadan{zháadin}    & to menopause \\

    \end{tabular}
    ~\\

    \subsection*{Examples}

        ~\\
        \begin{tabular}{l l}
            \laadan{Bíi áya hesh wa.}       & The grass is beautiful.          \\
            \laadan{Bíi áya lom wa.}        & The song is beautiful.          \\
            \laadan{Bíi áya thili wa.}      & The fish is beautiful.         \\
            \laadan{Bíi áya wíi wa.}        & Being alive is beautiful.          \\
            \laadan{Bíi áana with wa.}      & The woman sleeps.          \\
            \laadan{Bíi áana mid wa.}       & The animal sleeps.          \\
            \laadan{Bíi áana muda wa.}      & The pig sleeps.          \\
        \end{tabular}
        ~\\

        Did you notice, in the middle example, that a word that had been introduced as a verb, ``wíi'' (to be alive), was used as a noun, ``wíi'' (being alive).

        English does the same thing (known grammatically as ''nominalization``), forming ''abandonment`` from ``to abandon,'' ``carelessness'' from ``to be careless,'' and so on; any English verb can be used as a noun if ``–ing'' is added, as in ``Swimming is good exercise.''

        This process, which is common to all human languages, is very easy in Láadan. Simply use the verb in the noun's place in the sentence and add a case suffix to it (in this case the Subject Case suffix, which is a ``null suffix''); this will become clear in subsequent lessons.

    ~\\
    \begin{tabular}{l l}
        \laadan{Bíi hal with wa.}           & The woman works. \\
        \laadan{Bíi óoha with wa.}          & The woman is weary \\
        \laadan{Bíi hal i óoha with wa.}    & The woman works and is weary. \\
    \end{tabular}
    ~\\

    \section*{Exercises}

    \subsection*{Translate the following into English.}

        \begin{enumerate}
            \item[1.]  \laadan{Bíi áya bud wa.}                 ~\\ ~\\ English: \fitb{10cm}
            \item[2.]  \laadan{Bíi héeya thili wa.}             ~\\ ~\\ English: \fitb{10cm}
            \item[3.]  \laadan{Bíi wíi hesh wa.}                ~\\ ~\\ English: \fitb{10cm}
            \item[4.]  \laadan{Bíi áya ede wa.}                 ~\\ ~\\ English: \fitb{10cm}
            \item[5.]  \laadan{Bíi mime thul wa.}               ~\\ ~\\ English: \fitb{10cm}
            \item[6.]  \laadan{Bíi áya shum wa.}                ~\\ ~\\ English: \fitb{10cm}
            \item[7.]  \laadan{Bíi doth thili wa.}              ~\\ ~\\ English: \fitb{10cm}
            \item[8.]  \laadan{Bíi zháadin with wa.}            ~\\ ~\\ English: \fitb{10cm}
            \item[9.]  \laadan{Bíi áya ud wa.}                  ~\\ ~\\ English: \fitb{10cm}
            \item[10.] \laadan{Bíi óoha i áana Therísha wa.}    ~\\ ~\\ English: \fitb{10cm}
        \end{enumerate}

    \subsection*{Translate the following into Láadan.}
        \begin{enumerate}
            \item[11.]  \laadan{The door is open.}      ~\\ ~\\ Láadan: \fitb{10cm}
            \item[12.]  \laadan{The parent sleeps.}     ~\\ ~\\ Láadan: \fitb{10cm}
            \item[13.]  \laadan{The fish is tired.}     ~\\ ~\\ Láadan: \fitb{10cm}
            \item[14.]  \laadan{The pig follows.}       ~\\ ~\\ Láadan: \fitb{10cm}
            \item[15.]  \laadan{The woman asks.}        ~\\ ~\\ Láadan: \fitb{10cm}
            \item[16.]  \laadan{The song is beautiful.} ~\\ ~\\ Láadan: \fitb{10cm}
            \item[17.]  \laadan{The animal is afraid.}  ~\\ ~\\ Láadan: \fitb{10cm}
            \item[18.]  \laadan{The circle is closed.}  ~\\ ~\\ Láadan: \fitb{10cm}
            \item[19.]  \laadan{Mary menopauses.}       ~\\ ~\\ Láadan: \fitb{10cm}
            \item[20.]  \laadan{The grain is alive.}    ~\\ ~\\ Láadan: \fitb{10cm}
        \end{enumerate}

    \subsection*{Solutions}

        \begin{solution}
            \begin{tabular}{p{3.3cm}  p{3.3cm}  p{3.3cm}}
                1. The clothing is beautiful. &
                2. The fish is afraid (the fish fears). &
                3. The grass is alive. \\ \\
                4. The grain is beautiful. &
                5. The parent asks. &
                6. The air is beautiful. \\ \\
                7. The fish follows. &
                8. The woman menopauses. &
                9. The stone is beautiful. \\ \\
                10. Teresa is tired and sleeps & &
                
                \\ \\

                11. Bíi u áath wa.  &
                12. Bíi áana thul wa. \\ \\
                13. Bíi óoha thili wa.  &
                14. Bíi doth muda wa. &
                15. Bíi mime with wa. \\ \\
                16. Bíi áya lom wa. &
                17. Bíi héeya mid wa. &
                18. Bíi rahu tham wa. \\ \\
                19. Bíi zháadin Méri wa. &
                20. Bíi wíi ede wa. &
            \end{tabular}
        \end{solution}

    % -- LESSON ------------------------------------------------------ %
    \chapter{Plural}
        \section*{Vocabulary}
        ~\\
        \begin{tabular}{l p{10cm}}
            \laadan{dathim}     & to needlework \\
            \laadan{di}         & to say; to talk; to speak \\
            \laadan{–id}        & Suffix: male \\
            \laadan{le}         & I (first person pronoun, singular) \\
            \laadan{lezh}       & we (first person pronoun, several: 2 to 5) \\
            \laadan{len}        & we (first person pronoun, many: 6 or more) \\
            \laadan{me–}        & Prefix (on verb): plural \\
            \laadan{míi}        & to be amazed \\
            \laadan{thal}       & to be good \\
            \laadan{wáa}        & Evidence Word: assumed true by speaker because speaker trusts source \\
        \end{tabular}
        ~\\
        
        Pronouns, as in English, fill the position of nouns and are treated, grammatically, exactly like nouns.
        
        In connected sentences uttered by the same speaker where the Evidence Word would not change (``my perception'' vs ``trusted report'') from sentence to sentence, it may be omitted after the first sentence.
        
        The male suffix, ``–id,'' defines the noun or pronoun to which it is applied as male. Without it, the noun or pronoun is gender-neutral. So, to specify ``man'' we would use ``with'' (person) and apply the male suffix, giving ``withid.'' Since Láadan provides this mechanism for making a noun phrase explicitly male but none for making it explicitly female, the noun phrase without the ``–id'' suffix can be translated as female, barring other factors that make that translation inappropriate. Hence, you may see “with” translated as ``woman'' as often as you will as ``person.''
        
        \section*{Plural}
            
            \begin{center}
            [Verb CP–S]   
            \end{center}
            
            In this and subsequent Pattern models, we will assume that the Type-of-Sentence Word is present at the beginning of the model sentence and that the Evidence Word is at the end. With that assumption (and allowing for abbreviations) you will notice that this model is exactly the same as that in Lesson 2.
            
            To make a sentence plural in Láadan, only the verb is affected. To make a verb plural, put the prefix \laadan{``me–''} at the beginning of the word. Notice that the shape of the noun phrase doesn’t change in the plural.
            
            Láadan insists that consonant sounds and vowel sounds occur in strict alternation. No two consonants may occur together, and no two vowels may occur together (unless one is high-toned and the other is normal-toned and they are otherwise identical—and they’re in the same root word). To accomplish this, Láadan inserts an \laadan{``h''} to separate two vowels or an \laadan{``e''} to separate two consonants. So, if the verb being made plural begins with a vowel, we must insert an \laadan{``h''} between its initial vowel and the final \laadan{``e''} of \laadan{``me–.''}
            
            There is a variant form which is also correct. When the verb being made plural begins with a \laadan{``d,''} it can be pluralized by the use of the variant plural prefix \laadan{``n–.''} This \laadan{``n–''} is known as a ``syllabic n;'' it is a syllable unto itself—like the last syllable of the English word ``button.'' As a syllable unto itself, no inserted \laadan{``e''} will be necessary to separate it from the following \laadan{``d.''}


        \subsection*{Examples}

            ~\\
            \begin{tabular}{l l}
                \laadan{Bíi hal le wa.}     & I work. \\
                \laadan{Bíi mehal lezh wa.} & We (few) work. \\
                \laadan{Bíi mehal len wa.}  & We (many) work.
            \end{tabular}
            ~\\
            
            Note that the plural form of the verb is used with both the ``few/several'' form and the ``many'' form of a pronoun.
            
            ~\\
            \begin{tabular}{l p{10cm}}
                \laadan{Bíi di withid wáa.}     & The man speaks (I’m reliably informed). \\
                \laadan{Bíi ndi withid wáa.}    & The men speak (I’m told). \\
                \laadan{Bíi medi withid wáa.}   & The men speak (I’m told). \\
                \laadan{Bíi áya with wa.}       & The woman is beautiful (my perception). \\
                \laadan{Bíi meháya with wa.}    & The women are beautiful (my perception).
            \end{tabular}
            ~\\
            
            Note the \laadan{``h''} that has been inserted to separate the final \laadan{``e''} in \laadan{``me-''} from the initial \laadan{``á''\laadan} in \laadan{``áya.''} Also note that it wasn’t necessary when pluralizing \laadan{``hal,''} which begins with a consonant, \laadan{``h.''}
            
            ~\\
            \begin{tabular}{l p{7cm}}
                \laadan{Bíi áya i thal with wa.}        & The woman is beautiful and good (my perception). \\
                \laadan{Bíi meháya i methal with wa.}   & The women are beautiful and good (my perception).
            \end{tabular}
            ~\\
            
            Notice the compound verbs in the examples in the set above. When the sentence is plural, of course, both verbs have to be plural. Compound case phrases are just as easy to form, as you’ll see below.
            
            ~\\
            \begin{tabular}{l p{7cm}}
                \laadan{Bíi meháya thom i hesh wa.}         & The pillow and the grass are beautiful. \\
                \laadan{Bíi mezháadin Ána i Bétheni wáa.}   & Ann and Bethany are menopausing (I’m told).
            \end{tabular}
            ~\\
            
            Of course, with a compound Subject, the verb must be plural.

        \section*{Exercises}
            \subsection*{Make the following sentences singular; translate into English before and after.}

            \begin{enumerate}
                \item[1.]  \laadan{Bíi memíi thul wáa.}                 ~\\ ~\\ Singular: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[2.]  \laadan{Bíi mezháadin with wáa.}             ~\\ ~\\ Singular: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[3.]  \laadan{Bíi meháya thili wa.}                ~\\ ~\\ Singular: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[4.]  \laadan{Bíi methal thom wa.}                 ~\\ ~\\ Singular: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[5.]  \laadan{Bíi meháana lezh wa.}                ~\\ ~\\ Singular: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[6.]  \laadan{Bíi medoth hena wa.}                 ~\\ ~\\ Singular: \fitb{10cm} ~\\ English: \fitb{10cm}
            \end{enumerate}
            
            \subsection*{Make the following sentences plural; translate into English before and after.}
            
            \begin{enumerate}
                \item[7.]   \laadan{Bíi héeya mid wa.}       ~\\ ~\\ Plural: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[8.]   \laadan{Bíi wíi mudahid wáa.}    ~\\ ~\\ Plural: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[9.]   \laadan{Bíi hal withid wa.}      ~\\ ~\\ Plural: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[10.]  \laadan{Bíi thal rana wáa.}      ~\\ ~\\ Plural: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[11.]  \laadan{Bíi di le wa.}           ~\\ ~\\ Plural: \fitb{10cm} ~\\ English: \fitb{10cm}
                \item[12.]  \laadan{Bíi rahu áath wa.}       ~\\ ~\\ Plural: \fitb{10cm} ~\\ English: \fitb{10cm}
            \end{enumerate}
            
            \subsection*{Translate these into Láadan (all according to your own perception).}

            \begin{enumerate}
                \item[13.]  The pigs are tired.                 ~\\ ~\\ Láadan: \fitb{10cm}
                \item[14.]  The songs are good.                 ~\\ ~\\ Láadan: \fitb{10cm}
                \item[15.]  Carol and Matthew needlework.       ~\\ ~\\ Láadan: \fitb{10cm}
                \item[16.]  We (many) speak.                    ~\\ ~\\ Láadan: \fitb{10cm}
                \item[17.]  The stones are beautiful.           ~\\ ~\\ Láadan: \fitb{10cm}
                \item[18.]  The mothers are menopausing.        ~\\ ~\\ Láadan: \fitb{10cm}
            \end{enumerate}
            
        \subsection*{Solutions}
        \begin{solution}
            \begin{tabular}{p{3.3cm}  p{3.3cm}  p{3.3cm}}
                1. The parents are amazed. Bíi míi thul wáa. The parent is amazed. &
                2. The women are menopausing. Bíi zháadin with wáa. The woman is menopausing. &
                3. The fish are beautiful. Bíi áya thili wa. The fish is beautiful. \\ \\
                
                4. The pillows are good. Bíi thal thom wa. The pillow is good. &
                5. We (few) sleep. Bíi áana le wa. I sleep. &
                6. The siblings follow. Bíi doth hena wa. The sibling follows. 
                
                \\ \\

                7. The animal is afraid. Bíi mehéeya mid wa. The animals are afraid. &
                8. The boar (male pig) is alive. Bíi mewíi mudahid wáa. The boars are alive. &
                9. The man works. Bíi mehal withid wa. The men work. \\ \\
                
                10. The drink is good. Bíi methal rana wáa. The drinks are good. & 
                11. I speak. Bíi medi len (lezh) wa. We (many/few) speak. &
                12. The door is closed. Bíi merahu áath wa. The doors are closed.  
                
                \\ \\

                13. Bíi mehóoha muda wa. &
                14. Bíi methal lom wa. &
                15. Bíi medathim Hérel i Máthu wa. \\  \\
                
                16. Bíi ndi len wa. &
                17. Bíi meháya ud wa. &
                18. Bíi mezháadin thul wa. 
            \end{tabular}
        \end{solution}

    % -- LESSON ------------------------------------------------------ %
    \chapter{Negative}
        \section*{Vocabulary}
        ~\\
        \begin{tabular}{l p{10cm}}
            \laadan{bal}    & bread \\
            \laadan{dóon}   & to be correct \\
            \laadan{em}     & yes \\
            \laadan{laya}   & to be red \\
            \laadan{ne}     & you (second person pronoun, singular) \\
            \laadan{nezh}   & you (second person pronoun, several: 2 to 5) \\
            \laadan{née}    & to be alien \\
            \laadan{nen}    & you (second person pronoun, many: 6 or more) \\
            \laadan{ra}     & negative, no, not \\
            \laadan{wi}     & Evidence Word: known to the speaker because the matter is self-evident to the speaker and the hearer \\
        \end{tabular}
        ~\\

        \section*{Negative}
            
            \begin{center}
            [Verb (Negative) CP–S]
            \end{center}
            
            The parentheses around ``Negative'' mean that it is an optional element in the sentence.

            To make a sentence negative, just put \laadan{``ra''} immediately after the verb. The \laadan{``ra''} does not change the form of the Verb, nor of the Case Phrases.

            \laadan{``Ra''} is a very useful word. In addition to making a sentence negative, \laadan{``ra''} can also be used as a prefix on other words to make them mean their opposites. We’ve seen one example of this already: \laadan{``rahu''} (to be closed) comes from \laadan{``ra-''} (non–, not–) + \laadan{``u''} (to be open).
            

        \subsection*{Examples}

            ~\\
            \begin{tabular}{l p{10cm}}
                \laadan{Bíi thal with wa.}      & The woman is good (according to my perceptions). \\
                \laadan{Bíi thal ra mid wáa.}   & The creature is not good (I’m told). \\ \\
                
                \laadan{Bíi di ne wáa.}         & You speak (I’m told). \\
                \laadan{Bíi di ra mid wi.}      & The creature doesn’t speak (obviously). \\ \\
                
                \laadan{Bíi laya bal wáa.}      & The bread is red (I hear from a trusted source). \\
                \laadan{Bíi laya ra bal wa.}    & The bread is not red (according to my perceptions). \\
                \laadan{Bíi laya ra bal wi.}    & The bread is not red (obviously—because it is present for the speaker and hearer to examine). \\ \\
                
                \laadan{Bíi hal le wa.}         & I work. \\
                \laadan{Bíi hal ra le wa.}      & I don't work. \\
                \laadan{Bíi mehal lezh wa.}     & We (few) work. \\
                \laadan{Bíi mehal ra len wa.}   & We (many) do not work. \\ \\
            \end{tabular}
            ~\\

        \section*{Exercises}
            \subsection*{Translate the following into English.}

            \begin{enumerate}
                \item[1.]  \laadan{abc}                 ~\\ ~\\ English: \fitb{10cm}
            \end{enumerate}
            
            \subsection*{Make the following sentences negative; translate into English before and after.}

            \begin{enumerate}
                \item[1.]  \laadan{abc}                 ~\\ ~\\ English: \fitb{10cm}
            \end{enumerate}
            
            \subsection*{Translate the following into Láadan (they’re trusted hearsay, unless that doesn’t
make sense).}

            \begin{enumerate}
                \item[1.]  \laadan{Bíi mehóoha ra len wa.}      ~\\ ~\\ English: \fitb{10cm}
                \item[2.]  \laadan{Bíi née ra mid wáa.}         ~\\ ~\\ English: \fitb{10cm}
                \item[3.]  \laadan{Bíi u ra áath wi.}           ~\\ ~\\ English: \fitb{10cm}
                \item[4.]  \laadan{Bíi medi ra nezh wáa.}       ~\\ ~\\ English: \fitb{10cm}
                \item[5.]  \laadan{Bíi medoth ra thili wa.}     ~\\ ~\\ English: \fitb{10cm}
                \item[6.]  \laadan{Bíi mezháadin ra with wáa.}  ~\\ ~\\ English: \fitb{10cm}
            \end{enumerate}
          
        \subsection*{Solutions}
        \begin{solution}
            \begin{tabular}{p{3.3cm}  p{3.3cm}  p{3.3cm}}
                1. a &
                2. b &
                3. c \\
                
                \\ \\

                4. a &
                5. b &
                6. c \\
            \end{tabular}
        \end{solution}

    % -- LESSON ------------------------------------------------------ %
    \chapter{Template}
        \section*{Vocabulary}
        ~\\
        \begin{tabular}{l p{10cm}}
            \laadan{abc}    & def \\
        \end{tabular}
        ~\\

        \section*{Topic}
            asdfasdf

        \subsection*{Examples}

            ~\\
            \begin{tabular}{l p{10cm}}
                \laadan{abc} & def \\
            \end{tabular}
            ~\\

        \section*{Exercises}
            \subsection*{DoAThing}

            \begin{enumerate}
                \item[1.]  \laadan{abc}                 ~\\ ~\\ English: \fitb{10cm}
            \end{enumerate}

        \subsection*{Solutions}
        \begin{solution}
            \begin{tabular}{p{3.3cm}  p{3.3cm}  p{3.3cm}}
                1. a &
                2. b &
                3. c \\
                
                \\ \\

                4. a &
                5. b &
                6. c \\
            \end{tabular}
        \end{solution}
\end{document}
